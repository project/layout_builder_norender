<?php

namespace Drupal\layout_builder_norender\EventSubscriber;

use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\layout_builder\Event\SectionComponentBuildRenderArrayEvent;
use Drupal\layout_builder\LayoutBuilderEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Sets no render attribute.
 */
class SectionComponentNorender implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * {@inheritDoc}
   */
  public static function getSubscribedEvents() {
    // Run before BlockComponentRenderArray (priority 100), so that we can
    // hide the component (by stopping propagation) or, in the preview, show
    // the user a message about the component being hidden.
    $events[LayoutBuilderEvents::SECTION_COMPONENT_BUILD_RENDER_ARRAY] = ['onBuildRender', 501];
    return $events;
  }

  /**
   * Stops propagation if the component is unpublished for non preview, or gives message in preview.
   *
   * @param \Drupal\layout_builder\Event\SectionComponentBuildRenderArrayEvent $event
   *   The section component build render array event.
   */
  public function onBuildRender(SectionComponentBuildRenderArrayEvent $event) {
    $norender = $event->getComponent()->get('norender') ?: FALSE;
    if (!$norender) {
      return;
    }

    if ($event->inPreview()) {
      $block = $event->getPlugin();
      if (!$block instanceof BlockPluginInterface) {
        return;
      }

      // Set specific class for layout preview "norender" block.
      $build = [
        '#theme' => 'block',
        '#configuration' => $block->getConfiguration(),
        '#plugin_id' => $block->getPluginId(),
        '#base_plugin_id' => $block->getBaseId(),
        '#derivative_plugin_id' => $block->getDerivativeId(),
        '#weight' => $event->getComponent()->getWeight(),
        '#attributes' => [
          'class' => [
            'layout-builder-block--unpublished',
            ],
        ],
        '#attached' => [
          'library' => ['layout_builder_norender/block_norender']
        ],
        'content' => $block->build(),
      ];

      $event->setBuild($build);
    }

    $event->stopPropagation();
  }

}
