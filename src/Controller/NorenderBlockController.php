<?php

namespace Drupal\layout_builder_norender\Controller;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\layout_builder\Controller\LayoutRebuildTrait;
use Drupal\layout_builder\LayoutTempstoreRepositoryInterface;
use Drupal\layout_builder\SectionStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller for norender a component.
 */
class NorenderBlockController implements ContainerInjectionInterface {

  use LayoutRebuildTrait;

  /**
   * The layout tempstore repository.
   *
   * @var \Drupal\layout_builder\LayoutTempstoreRepositoryInterface
   */
  protected $layoutTempstoreRepository;

  /**
   * LayoutController constructor.
   *
   * @param \Drupal\layout_builder\LayoutTempstoreRepositoryInterface $layout_tempstore_repository
   *   The layout tempstore repository.
   */
  public function __construct(LayoutTempstoreRepositoryInterface $layout_tempstore_repository) {
    $this->layoutTempstoreRepository = $layout_tempstore_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('layout_builder.tempstore_repository')
    );
  }

  /**
   * Shows or unpublished a component.
   *
   * @param \Drupal\layout_builder\SectionStorageInterface $section_storage
   *   The section storage.
   * @param $delta
   *   The section delta.
   * @param $region
   *   The component region.
   * @param $uuid
   *   The component uuid.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  public function build(SectionStorageInterface $section_storage, $delta, $region, $uuid) {

    $section = $section_storage->getSection($delta);
    $component = $section->getComponent($uuid);
    $norender = $component->get('norender') ?: FALSE;
    $component->set('norender', !$norender);

    $this->layoutTempstoreRepository->set($section_storage);

    return $this->rebuildLayout($section_storage);
  }

}
