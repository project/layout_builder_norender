<?php

namespace Drupal\layout_builder_norender\Element;

use Drupal\Core\Security\TrustedCallbackInterface;

/**
 * LayoutBuilder
 */
class NorenderLayoutBuilder implements TrustedCallbackInterface {

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return ['preRender'];
  }

  /**
   * Add metadata for new contextual link.
   *
   * @param $element
   * @return array
   */
  public static function preRender($element) {
    // Add metadata about the current operations available in
    // contextual links. This will invalidate the client-side cache of
    // links that were cached before the 'block_norender' link was added.
    if (isset($element['layout_builder'])) {
      foreach ($element['layout_builder'] as $key => $el) {
        if (isset($el['layout-builder__section'])) {
          if (isset($el['layout-builder__section']['content'])) {
            foreach ($el['layout-builder__section']['content'] as $key_content => $content) {
              if (isset($content['#contextual_links'])) {
                if (isset($content['#contextual_links']['layout_builder_block'])) {
                  $element['layout_builder'][$key]['layout-builder__section']['content'][$key_content]['#contextual_links']['layout_builder_block']['metadata']['operations'] = 'move:update:remove:block_norender';
                }
              }
            }
          }
        }
      }
    }

    return $element;
  }

}
