# Layout Builder "Publish"/"Unpublish" (Render/No-render)

CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements  
* Installation
* Configuration
* Maintainers


INTRODUCTION
------------

This module is helper module for Drupal core layout builder module.
This module provides new contextual link for layout builder preview for each block/component. New contextual link "Publish/Unpubish" 
does NOT publish or unpublish given component, but with this option component is NOT rendered on page, but is still rendered on preview 
page for content editors to see how it would look like on page.

On preview page component has 'Unpublished' text in red with overlay over component to indicate to content editor that this 
block is not going to be rendered outside of layout edit (preview). On hover over "hidden" component, overlay is removed so 
content editors can see how component/block will look like when rendered to end user.

REQUIREMENTS
------------

This module requires:
1. Layout Builder (core module)

INSTALLATION
------------

Install the Layout Builder "Publish"/"Unpublish" (Render/No-render) module as you would normally install
any Drupal contrib module.

Visit [Installing Drupal Modules](https://www.drupal.org/node/1897420) for further information.

MAINTAINERS
-----------

Module was created by:

* Antonija Arbanas (agolubic) - https://www.drupal.org/u/agolubic